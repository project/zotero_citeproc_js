<?php

namespace Drupal\zotero_citeproc_js\Form;

use Drupal\auto_config_form\AutoConfigFormBase;

/**
 * The settings form.
 *
 * @package Drupal\zotero_citeproc_js\Form
 */
class SettingsForm extends AutoConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getSchemaKey(): string {
    return 'zotero_citeproc_js.settings';
  }

}
