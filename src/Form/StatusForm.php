<?php

namespace Drupal\zotero_citeproc_js\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\zotero_citeproc_js\Service\ZoteroCiteprocJsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Throwable;

/**
 * Class StatusForm.
 *
 * @package Drupal\zotero_citeproc_js\Form
 */
class StatusForm extends FormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The Zotero CiteProc JS service.
   *
   * @var \Drupal\zotero_citeproc_js\Service\ZoteroCiteprocJsService
   */
  protected $service;

  /**
   * Status form constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\zotero_citeproc_js\Service\ZoteroCiteprocJsService $service
   *   The Zotero CiteProc JS service.
   */
  public function __construct(
    Messenger $messenger,
    ZoteroCiteprocJsService $service
  ) {
    $this->messenger = $messenger;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('zotero_citeproc_js.server')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $data = <<<'JSON'
{
  "id": "ITEM-1",
  "type": "article-journal",
  "title": "'Let the dead bury their dead' (Matt. 8:22/Luke 9:60): Jesus and the halakhah",
  "container-title": "The Journal of Theological Studies",
  "page": "553-581",
  "volume": "49",
  "issue": "2",
  "URL": "https://www.jstor.org/stable/23968765",
  "author": [
    {
      "family": "Bockmuehl",
      "given": "Markus"
    }
  ],
  "issued": {
    "date-parts": [
      [
        1998,
        10
      ]
    ]
  },
  "publisher": [
    {
      "literal": "Oxford University Press"
    }
  ]
}
JSON;

    try {
      $citation = $this->service->renderSingleCitation(
        json_decode($data),
        'society-of-biblical-literature-fullnote-bibliography',
        'en-US'
      );
    }
    catch (Throwable $e) {
      $citation = $e;
    }
    $explanation = $this->t('If you see a rendered citation below, the service is working.');
    $form['test'] = [
      '#type' => 'markup',
      '#prefix' => '<h3>Test</h3><div>',
      '#suffix' => '</div>',
      '#markup' => "<p>$explanation</p><blockquote><pre>$citation</pre></blockquote>",
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do here.
  }

}
