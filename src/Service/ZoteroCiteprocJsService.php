<?php

namespace Drupal\zotero_citeproc_js\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * A Drupal service that calls an external Zotero CiteprocJS service.
 */
class ZoteroCiteprocJsService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Construct a ZoteroCiteprocJsService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory;
  }

  /**
   * Render the given CSL JSON data using a Zotero CiteProc JS server.
   *
   * @param mixed $body
   *   The body of the HTTP POST request. Will be JSON-encoded.
   * @param mixed $query
   *   The query parameters to use in the URL.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the service, verbatim.
   *
   * @see https://github.com/zotero/citeproc-js-server#using-the-web-service
   */
  public function call($body, $query): ResponseInterface {
    $config = $this->config->get('zotero_citeproc_js.settings');
    $client = new Client([
      'base_uri' => $config->get('url'),
      'timeout' => $config->get('timeout'),
    ]);
    return $client->post('', [
      'query' => $query,
      'body' => json_encode($body),
    ]);
  }

  /**
   * Render a single citation.
   *
   * @param object $item
   *   A JSON-serializable item in CSL JSON format.
   * @param string $style
   *   The name of the CSL style sheet to use.
   * @param string $locale
   *   The name of the locale to use.
   *
   * @return string
   *   The rendered citation.
   *
   * @throws \RuntimeException
   */
  public function renderSingleCitation(
    object $item,
    string $style,
    string $locale
  ): string {
    if (empty($item->id)) {
      $item->id = "ITEM-1";
    }
    $body = [
      'items' => [
        $item->id => $item,
      ],
    ];
    $query = [
      'bibliography' => 0,
      'citations' => 1,
      'style' => $style,
      'locale' => $locale,
    ];
    $result = json_decode($this->call($body, $query)->getBody()->getContents());
    return $result->citations[0][1];
  }

}
