<?php

namespace Drupal\Tests\zotero_citeproc_js\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test whether we can install our module without breaking the site.
 *
 * @group zotero_citeproc_js
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['zotero_citeproc_js'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Make sure the site still works.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLoadFront() {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Log in');
  }

}
