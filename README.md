# Zotero CiteProc JS Server Integration

Integrates Drupal with a Zotero CiteProc JS server.

To set up such a server, read [the official docs](https://hub.docker.com/r/refstudycentre/citeproc-js-server) or use
our [docker container](https://hub.docker.com/r/refstudycentre/citeproc-js-server).
